﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace O365Phonebook
{
    class ExchangeHelper : IDisposable
    {
        private ExchangeService ExchangeService { get; set; }

        private string username;

        public ExchangeHelper(string username, string password)
        {
            ExchangeService = new ExchangeService()
            {
                Credentials = new WebCredentials(username, password)
            };

            this.username = username;
        }

        public void Autodiscover()
        {
            ExchangeService.AutodiscoverUrl(this.username, (redirectionUrl) =>
            {
                // The default for the validation callback is to reject the URL.
                bool result = false;

                Uri redirectionUri = new Uri(redirectionUrl);

                if (redirectionUri.Scheme == "https")
                {
                    result = true;
                }

                return result;
            });
        }

        public List<Models.PhonebookEntry> GetContacts()
        {
            // Bind to Exchange Contacts folder
            var contactsFolder = ContactsFolder.Bind(ExchangeService,
                                                     WellKnownFolderName.Contacts,
                                                     new PropertySet(BasePropertySet.IdOnly, FolderSchema.TotalCount));

            var pageSize = 35;
            var itemViewOffset = 0;

            var entries = new List<Models.PhonebookEntry>();

            // Page through all the results
            while (true)
            {
                var itemView = new ItemView(pageSize, itemViewOffset);
                itemView.PropertySet = PhonenumbersPropertySet;

                var results = ExchangeService.FindItems(WellKnownFolderName.Contacts, itemView);
                foreach (var r in results)
                {
                    if (!(r is Contact)) continue;

                    var contact = (Contact)r;

                    // Add all types of phonenumbers
                    foreach (var ps in PhonenumberSuffixes)
                    {
                        if (contact.PhoneNumbers.Contains(ps.Key) && contact.PhoneNumbers[ps.Key] != null)
                        {
                            entries.Add(new Models.PhonebookEntry()
                            {
                                DisplayName = contact.DisplayName + (!string.IsNullOrEmpty(ps.Value) ? " (" + ps.Value + ")" : ""),
                                PhoneNumber = contact.PhoneNumbers[ps.Key].Replace(" ", "")
                            });
                        }
                    }
                }

                if (results.Items.Count == pageSize)
                {
                    // This was a full page, add to the offset and loop again
                    itemViewOffset += pageSize;
                }
                else
                {
                    // This wasn't a full page, we've had all the results, stop.
                    break;
                }
            }

            return entries.OrderBy(e => e.DisplayName).ToList();
        }

        public void Dispose()
        {

        }

        private readonly PropertySet PhonenumbersPropertySet = new PropertySet(
            BasePropertySet.IdOnly, ContactSchema.DisplayName,
            ContactSchema.MobilePhone, ContactSchema.CarPhone, ContactSchema.HomePhone,
            ContactSchema.HomePhone2, ContactSchema.BusinessPhone, ContactSchema.BusinessPhone2,
            ContactSchema.BusinessFax, ContactSchema.OtherFax, ContactSchema.HomeFax, ContactSchema.Pager,
            ContactSchema.OtherTelephone, ContactSchema.Callback, ContactSchema.CompanyMainPhone, ContactSchema.PrimaryPhone,
            ContactSchema.AssistantPhone, ContactSchema.RadioPhone, ContactSchema.TtyTddPhone, ContactSchema.Telex);

        private readonly Dictionary<PhoneNumberKey, string> PhonenumberSuffixes = new Dictionary<PhoneNumberKey, string> {
            { PhoneNumberKey.MobilePhone, "Mobile" },
            { PhoneNumberKey.CarPhone, "Car" },
            { PhoneNumberKey.HomePhone, string.Empty },
            { PhoneNumberKey.HomePhone2, string.Empty },
            { PhoneNumberKey.BusinessPhone, string.Empty },
            { PhoneNumberKey.BusinessPhone2, string.Empty },
            { PhoneNumberKey.BusinessFax, "Fax" },
            { PhoneNumberKey.OtherFax, "Fax" },
            { PhoneNumberKey.HomeFax, "Fax" },
            { PhoneNumberKey.Pager, string.Empty },
            { PhoneNumberKey.OtherTelephone, string.Empty },
            { PhoneNumberKey.Callback, string.Empty },
            { PhoneNumberKey.CompanyMainPhone, string.Empty },
            { PhoneNumberKey.PrimaryPhone, string.Empty },
            { PhoneNumberKey.AssistantPhone, string.Empty },
            { PhoneNumberKey.RadioPhone, string.Empty },
            { PhoneNumberKey.TtyTddPhone, string.Empty },
            { PhoneNumberKey.Telex, "Telex" }
        };
    }
}