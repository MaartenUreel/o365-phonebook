﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace O365Phonebook
{
    public static class HttpContextExtensions
    {
        public static void ReturnWithStatus(this HttpContext application, HttpStatusCode statusCode, string message)
        {
            application.Response.Clear();
            application.Response.Write(message);
            application.Response.StatusCode = (int)statusCode;
            application.Response.End();
        }
    }
}