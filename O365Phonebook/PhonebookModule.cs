﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Net;
using System.Web;
using System.Linq;
using System.Xml.Linq;

namespace O365Phonebook
{
    public class PhonebookModule : IHttpModule
    {
        public void Dispose()
        {
            //clean-up code here.
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += Context_BeginRequest;
        }

        private void Context_BeginRequest(object sender, EventArgs e)
        {
            var application = (HttpApplication)sender;

            var requestURL = application.Request.Url;
            var pathTokens = requestURL.LocalPath.Trim('/').Split('/');

            // In all cases, URL should contain at least 3 tokens
            if (pathTokens.Length < 3)
            {
                application.Context.ReturnWithStatus(HttpStatusCode.BadRequest, "Invalid path");
                return;
            }

            // Determine whether it's a registered account URL or a direct URL
            var request = new Models.PhonebookRequest();
            if (pathTokens[0] == "direct")
            {
                // /direct/{username}/{password}/{feedType}/{p1}[/{q2},...]
                if (pathTokens.Length < 5)
                {
                    application.Context.ReturnWithStatus(HttpStatusCode.BadRequest, "Invalid path");
                    return;
                }

                request.Username = pathTokens[1];
                request.Password = pathTokens[2];
                request.FeedType = pathTokens[3];
                request.Parameters = pathTokens.Skip(4);
            }
            else
            {
                // /key/{feedType}/{p1}[/{q2},...]

                // Load XML file
                using (var sr = new System.IO.StreamReader(application.Server.MapPath("~/Accounts.xml")))
                {
                    var xDoc = XDocument.Load(sr, LoadOptions.None);

                    // Find the specified account by id
                    var accountNode = xDoc.Root
                        .Elements("account")
                        .FirstOrDefault(n => (string)n.Attribute("key") == pathTokens[0]);

                    if (accountNode == null)
                    {
                        // Key not found
                        application.Context.ReturnWithStatus(HttpStatusCode.NotFound, "Invalid key");
                        return;
                    }

                    // Assign data from XML
                    request.Username = accountNode.Element("username").Value;
                    request.Password = accountNode.Element("password").Value;
                }

                // Assign the rest from URL
                request.FeedType = pathTokens[1];
                request.Parameters = pathTokens.Skip(2);
            }

            // Find a handler for this feed type
            var allFeedTypeHandlers = from t in this.GetType().Assembly.GetTypes()
                                      where t.GetInterfaces().Contains(typeof(FeedTypes.IFeedType))
                                      select new
                                      {
                                          Type = t,
                                          FeedTypeAttributes = (FeedTypes.FeedTypeAttribute[])t.GetCustomAttributes(typeof(FeedTypes.FeedTypeAttribute), false)
                                      };

            var matchingFeedTypeHandlers = allFeedTypeHandlers.Where(h => h.FeedTypeAttributes.Any(a => a.URLSlug == request.FeedType));

            if (!matchingFeedTypeHandlers.Any())
            {
                // No handler found
                application.Context.ReturnWithStatus(HttpStatusCode.NotFound, "No valid feedType");
                return;
            }

            // The request can be processed, retrieve the data from O365
            var exchange = new ExchangeHelper(request.Username, request.Password);
            exchange.Autodiscover();
            var phonebookEntries = exchange.GetContacts();

            // Let one or more handlers process the list
            foreach (var h in matchingFeedTypeHandlers)
            {
                var handlerInstance = (FeedTypes.IFeedType)Activator.CreateInstance(h.Type);
                handlerInstance.Parse(application.Context, request, phonebookEntries);
            }

            application.Context.Response.End();
        }
    }
}
