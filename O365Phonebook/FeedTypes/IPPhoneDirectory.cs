﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using O365Phonebook.Models;
using System.Xml.Linq;

namespace O365Phonebook.FeedTypes
{
    [FeedType("snom")]
    [FeedType("yealink")]
    [FeedType("rtx")]
    public class IPPhoneDirectory : IFeedType
    {
        public void Parse(HttpContext context, PhonebookRequest request, List<PhonebookEntry> entries)
        {
            string rootElement;
            switch (request.FeedType)
            {
                case "snom":
                    rootElement = "SnomIPPhoneDirectory";
                    break;

                case "yealink":
                    rootElement = "YealinkIPPhoneDirectory";
                    break;

                default:
                    rootElement = "IPPhoneDirectory";
                    break;
            }

            var xDoc = new XDocument(
                new XElement(rootElement,
                    entries.Select(e => new XElement("DirectoryEntry",
                        new XElement("Name", e.DisplayName),
                        new XElement("Telephone", e.PhoneNumber)))
                    )
                 );

            context.Response.Write(xDoc);
            context.Response.ContentType = "text/xml";
        }
    }
}