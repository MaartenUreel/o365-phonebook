﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace O365Phonebook.FeedTypes
{
    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    sealed class FeedTypeAttribute : Attribute
    {
        public string URLSlug { get; set; }

        public FeedTypeAttribute(string URLSlug)
        {
            this.URLSlug = URLSlug;
        }

    }

}