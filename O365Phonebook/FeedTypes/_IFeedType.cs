﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace O365Phonebook.FeedTypes
{
    public interface IFeedType
    {
        void Parse(HttpContext context, Models.PhonebookRequest request, List<Models.PhonebookEntry> entries);
    }
}