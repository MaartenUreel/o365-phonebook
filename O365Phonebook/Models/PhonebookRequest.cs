﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace O365Phonebook.Models
{
    public class PhonebookRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string FeedType { get; set; }

        public IEnumerable<string> Parameters { get; set; }
    }
}