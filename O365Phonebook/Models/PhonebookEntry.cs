﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace O365Phonebook.Models
{
    public class PhonebookEntry
    {
        public string DisplayName { get; set; }

        public string PhoneNumber { get; set; }
    }
}