# O365 Phonebook
A bridge between Office365 (of services using Exchange Webservices in general) and various IP Phones, providing a phonebook that can be configured into your phone.

## Installation
Installation is pretty easy, just deploy the source code as an IIS site, running in Integrated mode (IIS 7 and up).
Everything is handled by the PhonebookModule from then on.

*Warning: run this as a seperate site, don't merge within an existing project since all traffic would go to the PhonebookModule and break your existing website.*

## Using
Once installed, there are two ways to access the O365 data:

**Directly, passing username and password in the URL:**

https://your_project_url/direct/your_username@example.com/your_password/snom/feed.xml
Where you replace your username and password off course.

**Using a key, storing the user data in an XML file:**

https://your_project_url/my_secret_key/snom/feed.xml
Where you would configure my_secret_key in Accounts.xml, as can be seen in Accounts.example.xml.

Please note that requesting a feed could take a while, because all contacts have to be retrieve from Exchange first. Especially the autodiscovery could take a few seconds.
It shouldn't matter because your phone will pull these feeds in the background.

## Currently supported feed types:
* Snom: https://your_project_url/my_secret_key/snom/feed.xml
* Yealink: https://your_project_url/my_secret_key/snom/feed.lst
* RTX: https://your_project_url/my_secret_key/snom/feed.xml

It doesn't matter what you use after the type of feed, it's ignored anyway. We used feed.xml or feed.lst in our example above, but anything would work.

In the source code, you'll see that all these feeds are handled by the same IPPhoneDirectory handler, because they look very similar.
The structure of the project however allows you to easily add an extra `IFeedType`, mark it with the `FeedTypeAttribute` and you would be able to serve a completely different structured file.

If you would like me to add more types, drop me a message and I will add it.

## Purpose of this project
The purpose of this project isn't really that you deploy it yourself, but that you could reuse parts of the code and logic to implement it yourself.
Most likely the exact feed composition wouldn't be exactly what you or your customer wants.

## Copyright
None whatsoever, do as you please.